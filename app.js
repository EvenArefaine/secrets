// Import required modules
require("dotenv").config();
const express = require("express");
const bodyParser = require("body-parser");
const ejs = require("ejs");
const mongoose = require("mongoose");
const session = require("express-session");
const passport = require("passport");
const passportLocalMongoose = require("passport-local-mongoose");
const GoogleStrategy = require("passport-google-oauth20").Strategy;
const FacebookStrategy = require("passport-facebook").Strategy;

// Create an Express application
const app = express();

const username = process.env.u_Name
const password = process.env.p_Word

// Serve static files from the "public" directory
app.use(express.static("public"));
// Set the view engine to EJS
app.set('view engine', 'ejs');
// Use body-parser to parse incoming request bodies
app.use(bodyParser.urlencoded({ extended: true }));

// A web application needs the ability to identify users as they browse from page to page. 
// This series of requests and responses, each associated with the same user, is known as a session.
app.use(session({
   secret: "Our little secret.",
   resave: false,
   saveUninitialized: false,
   cookie: {},
}));

app.use(passport.initialize());
app.use(passport.session());

// Connect to the database
mongoose.connect("mongodb+srv://" + username + ":" + password + "@cluster0.qbr6cwh.mongodb.net/userDB");

// Define the Mongoose schema for users
const userSchema = new mongoose.Schema({
   email: String,
   password: String,
   googleId: String,
   facebookId: String,
   secret: String
});

userSchema.plugin(passportLocalMongoose);

// Create a Mongoose model using the userSchema to define your users collection
const User = new mongoose.model("User", userSchema);

// The createStrategy is responsible to setup passport-local LocalStrategy with the correct options.
passport.use(User.createStrategy());

// Creats a cookie of the user identification
passport.serializeUser(function (user, done) {
   done(null, user);
});
// Allows passport to crumble the cookie of the user and discover the message inside that identifies the user so we can aunthenticate them in our server
passport.deserializeUser(function (user, done) {
   done(null, user);
});

// Configure Strategy
// The Google authentication strategy authenticates users using a Google account and OAuth 2.0 tokens. 
//The client ID and secret obtained when creating an application are supplied as options when creating the strategy. 
//The strategy also requires a verify callback, which receives the access token and optional refresh token, as well as profile which contains the authenticated user's Google profile. 
//The verify callback must call done providing a user to complete authentication.
passport.use(new GoogleStrategy({
   clientID: process.env.CLIENT_ID,
   clientSecret: process.env.CLIENT_SECRET,
   callbackURL: "https://secrets-16w1.onrender.com/auth/google/secrets"
},
   function (accessToken, refreshToken, profile, done) {
      User.findOne({ googleId: profile.id })
         .then((founduser) => {
            if (!founduser) {  //if no foundUser , we create one
               const newUser = new User({
                  username: profile.displayName,
                  googleId: profile.id
               })
               newUser.save() //save the User and then send back the newly created user
                  .then(() => {
                     done(null, newUser)
                  })
                  .catch((err) => {
                     done(err)
                  })
            } else { //if user already exist, just send back the user
               done(null, founduser)
            }
         })
         .catch((err) => {
            done(err)
         })
   }
));

//The Facebook authentication strategy authenticates users using a Facebook account and OAuth 2.0 tokens. 
//The app ID and secret obtained when creating an application are supplied as options when creating the strategy. 
//The strategy also requires a verify callback, which receives the access token and optional refresh token, as well as profile which contains the authenticated user's Facebook profile. 
//The verify callback must call done providing a user to complete authentication

passport.use(new FacebookStrategy({
   clientID: process.env.FACEBOOK_APP_ID,
   clientSecret: process.env.FACEBOOK_APP_SECRET,
   callbackURL: "https://secrets-16w1.onrender.com/auth/facebook/secrets"
},
   function (accessToken, refreshToken, profile, done) {
      User.findOne({ facebookId: profile.id })
         .then((founduser) => {
            if (!founduser) {  //if no foundUser , we create one
               const newUser = new User({
                  username: profile.displayName,
                  facebookId: profile.id
               })
               newUser.save() //save the User and then send back the newly created user
                  .then(() => {
                     done(null, newUser)
                  })
                  .catch((err) => {
                     done(err)
                  })
            } else { //if user already exist, just send back the user
               done(null, founduser)
            }
         })
         .catch((err) => {
            done(err)
         })
   }
));
// Handle GET requests to the root path
app.get("/", function (req, res) {
   res.render("home");
});

//Use passport.authenticate(), specifying the 'google' strategy, to authenticate requests.
app.get("/auth/google",
   passport.authenticate("google", { scope: ["profile"] })
);

app.get("/auth/google/secrets",
   passport.authenticate('google', { failureRedirect: "/login" }),
   function (req, res) {
      // Successful authentication, redirect to secrets page.
      res.redirect("/secrets");
   });

//Use passport.authenticate(), specifying the 'facebook' strategy, to authenticate requests.
app.get("/auth/facebook",
   passport.authenticate("facebook")
);

app.get("/auth/facebook/secrets",
   passport.authenticate('facebook', { failureRedirect: "/login" }),
   function (req, res) {
      // Successful authentication, redirect to secrets page.
      res.redirect("/secrets");
   });

// Handle GET requests to the login path
app.get("/login", function (req, res) {
   res.render("login");
});
// Handle GET requests to the register path
app.get("/register", function (req, res) {
   res.render("register");
});
// Handle GET requests to the secrets path
app.get("/secrets", function (req, res) {
   User.find({ "secret": { $ne: null } }).then(foundUsers => {
      res.render("secrets", { usersWithSecrets: foundUsers });
   }).catch(err => {
      console.log(err);
   })
});
// Handle GET requests to the sumbit path
app.get("/submit", function (req, res) {
   if (req.isAuthenticated()) {
      res.render("submit");
   }
   else {
      res.redirect("/login");
   }
});
// Handle GET requests to the logout path
app.get("/logout", function (req, res) {
   req.logout(function (err) {
      if (err) {
         console.log(err)
      }
      else {
         res.redirect("/");
      }
   });
});
// Define a route for handling POST requests to the register page.
app.post("/register", function (req, res) {

   User.register({ username: req.body.username }, req.body.password)
      .then(() => {
         passport.authenticate("local")(req, res, function () {
            res.redirect("/secrets");
         })
      })
      .catch(err => {
         console.log(err);
         res.redirect("/register");
      });
});
// Define a route for handling POST requests to the login page.
app.post("/login", function (req, res) {
   const user = new User({
      username: req.body.username,
      password: req.body.password
   })

   req.login(user, function (err) {
      if (err) {
         console.log(err);
      }
      else {
         passport.authenticate("local")(req, res, function () {
            res.redirect("/secrets");
         })
      }
   })
});

// Define a route for handling POST requests to the submit page.
app.post("/submit", function (req, res) {
   const submittedSecret = req.body.secret;
   //Once the user is authenticated and their session gets saved, their user details are saved to req.user.
   User.findById(req.user._id).then(foundUser => {
      if (foundUser) {
         foundUser.secret = submittedSecret;
         foundUser.save().then(() => {
            res.redirect("/secrets");
         }).catch(err => {
            console.log(err);
         })
      }
   }).catch(err => {
      console.log(err);
   })
});

let port = process.env.PORT;
if (port == null || port == "") {
   port = 3000;
}

app.listen(port, function () {
   console.log("Server has started successfully.");
});
